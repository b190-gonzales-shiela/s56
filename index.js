function countLetter(letter, sentence) {

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    let letter = (string) => {
      let count = 1;
      for (let i = 0; i < string.length; i++) {
        if (string[i] === string[i + 1]) {
          count++;
        } else {
          console.log(`${string[i]} occur ${count} times`);
          count = 1;
        }
      }
    };
     
    countLetter("hello");
 






function isIsogram(text) {

    
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

   var i, j;
   text = text.toLowerCase();
   for(i = 0; i < text.length; ++i) {
     for(j = i + 1; j < text.length; ++j) {
       if(text[i] === text[j]) {
         return false;
       }
     }
   }
   return true;
};



function purchase(age,price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
  
    const discounted=(price)*.20.toFixed(2);

    if (age>13){
        return ('undefined');
    } else if (age>13 && age>21) {
        return (price);
    } else if(age<64){
        return (discounted);
    }
    
}
purchase();

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
   const items = [
           { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
           { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
           { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
           { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
           { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
       ];


     let newArr = [];    
        let ilen = items.length
        for(let i = 0; i < ilen; i++){
          if(items[i].stocks === 0){
            newArr.push(items[i].category)
          }
       }
       return [...new Set(newArr)];
     }
     
   console.log(findHotCategories(items));


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
   let candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
   let candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

    const result = source.findFlyingVoters(candidateA, candidateB);
        expect(result).toEqual(['LIWf1l', 'V2hjZH']);
       
}



module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};   